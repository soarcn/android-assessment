package com.cocosw.android.service

import com.cocosw.android.service.APINAME.API
import com.cocosw.android.service.APINAME.DEFAULT
import com.squareup.moshi.Moshi
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

object APINAME {
    const val API = "Api"
    const val DEFAULT = "default"
}

val serviceModule = module {
    single { provideRetrofit(get(), get(named(API)), get()) }
    single { provideApiService(get()) }
    single { provideBaseUrl() }
    single(named(API)) { provideApiClient(get(named(DEFAULT))) }
    single { provideMoshi() }
}

private fun provideMoshi(): Moshi {
    return Moshi.Builder()
        .build()
}

private fun provideRetrofit(
    baseUrl: HttpUrl,
    client: OkHttpClient,
    moshi: Moshi
): Retrofit {
    return Retrofit.Builder() //
        .client(client) //
        .baseUrl(baseUrl) //
        .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient()) //
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()) //
        .build()
}

private fun provideApiService(retrofit: Retrofit): Api {
    return retrofit.create(Api::class.java)
}

private fun provideBaseUrl(): HttpUrl {
    return PRODUCTION_API_URL.toHttpUrlOrNull()!!
}

private fun provideApiClient(client: OkHttpClient): OkHttpClient {
    return createApiClient(client).build()
}

private fun createApiClient(client: OkHttpClient): OkHttpClient.Builder {
    return client.newBuilder()
}

private val PRODUCTION_API_URL = "https://bruce-v2-mob.fairfaxmedia.com.au/1/coding_test/"
