package com.cocosw.android.service

class ApiError(val code: Int, val message: String)
