package com.cocosw.android.service

import io.reactivex.Single
import retrofit2.http.GET

interface Api {
    @GET("13ZZQX/full")
    fun get(): Single<Articles>
}
