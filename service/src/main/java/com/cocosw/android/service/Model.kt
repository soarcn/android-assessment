package com.cocosw.android.service

import com.squareup.moshi.JsonClass
import java.io.Serializable

@JsonClass(generateAdapter = true)
class Asset(val id: String,
            val headline: String,
            val theAbstract: String,
            val byLine: String,
            val timeStamp: Long,
            val relatedImages: List<RelatedImage>?,
            val url: String) : Serializable

@JsonClass(generateAdapter = true)
class RelatedImage(
    val id: String,
    val url: String,
    val width: Int,
    val height: Int
) : Serializable, Comparable<RelatedImage> {
    override fun compareTo(other: RelatedImage) = (width * height - other.width * other.height)
}

@JsonClass(generateAdapter = true)
class Articles(
    val displayName: String,
    val assets: List<Asset>
) : Serializable