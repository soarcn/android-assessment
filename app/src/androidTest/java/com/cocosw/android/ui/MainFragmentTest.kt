package com.cocosw.android.ui

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.FragmentScenario
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.hasData
import androidx.test.espresso.intent.rule.IntentsTestRule
import com.cocosw.android.R
import com.cocosw.android.domain.Domain
import com.cocosw.android.mock
import com.cocosw.android.service.Api
import com.cocosw.android.service.MockArticles
import com.cocosw.optimus.MockResponseSupplier
import com.schibsted.spain.barista.assertion.BaristaRecyclerViewAssertions
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions
import com.schibsted.spain.barista.interaction.BaristaListInteractions
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.core.context.loadKoinModules
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject

class MainFragmentTest : KoinTest {

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val intentsTestRule: TestRule =
        IntentsTestRule(FragmentScenario.EmptyFragmentActivity::class.java)

    private val mock: MockResponseSupplier by inject()

    /**
     * Given Domain returns correct list with 14 assets
     * As a user
     * I'm able to see a grid with 18 article blocks on the UI
     */
    @Test
    fun happyPath() {
        mock.mock(Api::get, value = MockArticles::Success)
        launchFragment()
        BaristaRecyclerViewAssertions.assertRecyclerViewItemCount(R.id.list, 14)
        BaristaListInteractions.scrollListToPosition(R.id.list, 1)
        BaristaVisibilityAssertions.assertContains("Palmer forks")
    }

    /**
     * Given Domain returns a error
     * As a user
     * I will see a general error message on the screen
     */
    @Test
    fun unhappyPath() {
        val mockDomain: Domain = mockk()
        every { mockDomain.getModel() } returns Single.error(Exception())
        loadKoinModules(
            module(override = true) {
                factory { mockDomain }
            })
        launchFragment()
        BaristaRecyclerViewAssertions.assertRecyclerViewItemCount(R.id.list, 0)
        BaristaVisibilityAssertions.assertContains("Something went wrong")
    }

    /**
     * Given Domain returns a error
     * As a user
     * I will see a general error message on the screen
     */
    @Test
    fun test400Error() {
        mock.mock(Api::get, value = MockArticles::HTTP404)

        launchFragment()
        BaristaRecyclerViewAssertions.assertRecyclerViewItemCount(R.id.list, 0)
        BaristaVisibilityAssertions.assertContains("Something went wrong")
    }

    /**
     * Given user can see a article block in the list
     * As a user
     * If I click the card
     * Then I'm able to see the article open in a webview
     */
    @Test
    fun openUrl() {
        launchFragment()
        BaristaListInteractions.clickListItem(R.id.list, 1)
        intended(hasData(Uri.parse("http://www.afr.com/brand/chanticleer/goldman-sachs-readies-for-new-investment-banking-era-20201213-p56n82")))
    }

    private fun launchFragment() {
        launchFragmentInContainer<MainFragment>(themeResId = R.style.Theme_Android)
    }
}