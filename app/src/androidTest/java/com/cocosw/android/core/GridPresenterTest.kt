package com.cocosw.android.core

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.cocosw.android.domain.DomainModel
import com.cocosw.android.mockLifecycleOwner
import com.cocosw.android.ui.ListAdapter
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.test.KoinTest
import org.koin.test.get

class GridPresenterTest : KoinTest {

    private val adapter: ListAdapter = ListAdapter()

    @Rule
    @JvmField
    val rule: TestRule = InstantTaskExecutorRule()
    private lateinit var layout: RecyclerView
    private lateinit var presenter: GridPresenter<DomainModel>

    @Before
    fun setup() {
        layout = mockk(relaxed = true)
        every { layout.resources.getInteger(any()) } returns 2
        presenter = GridPresenter(layout, adapter, get())
    }

    @Test
    fun observe() {
        val liveData = MutableLiveData<List<DomainModel>>()
        presenter.observe(liveData, mockLifecycleOwner())
        verify {
            layout.setAdapter(adapter)
        }

        val list = listOf(
            DomainModel(
                "test", "test", "",
                "", "", 11, null
            )
        )
        liveData.value = list

        assertThat(adapter.data).hasSize(1)
        assertThat(adapter.data)
    }
}