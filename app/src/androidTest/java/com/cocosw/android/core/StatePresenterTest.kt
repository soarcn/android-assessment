package com.cocosw.android.core

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cocosw.android.mockLifecycleOwner
import com.cocosw.android.ui.MainViewModel
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.koin.test.KoinTest
import org.koin.test.get

class StatePresenterTest:KoinTest {

    @Rule @JvmField
    val rule: TestRule = InstantTaskExecutorRule()

    private lateinit var vm: MainViewModel
    private lateinit var layout: SwipeRefreshLayout
    private lateinit var presenter: StatePresenter

    @Before
    fun setup() {
        layout = mockk(relaxed = true)
        presenter = StatePresenter(layout)
        vm = MainViewModel(get())
        presenter.observe(vm, mockLifecycleOwner())
    }

    @Test
    fun onLoading() {
        vm.state.value = Status.LOADING
        verify { layout.setRefreshing(true) }
    }


    @Test
    fun onSuccess() {
        vm.state.value = Status.SUCCESS
        verify { layout.setRefreshing(false) }
    }
}