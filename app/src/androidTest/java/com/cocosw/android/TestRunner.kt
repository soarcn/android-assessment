package com.cocosw.android

import android.annotation.SuppressLint
import android.app.Application
import android.app.KeyguardManager
import android.content.Context
import android.os.PowerManager
import androidx.test.runner.AndroidJUnitRunner
import com.blankj.utilcode.util.Utils
import org.koin.core.context.loadKoinModules
import org.koin.test.KoinTest

class TestRunner : AndroidJUnitRunner(), KoinTest {
    private var wakeLock: PowerManager.WakeLock? = null

    @SuppressLint("MissingPermission")
    override fun onStart() {
        val app = targetContext.applicationContext
        val name = TestRunner::class.java.simpleName
        // Unlock the device so that the tests can input keystrokes.
        val keyguard = app.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        keyguard.newKeyguardLock(name).disableKeyguard()
        // Wake up the screen.
        val power = app.getSystemService(Context.POWER_SERVICE) as PowerManager
        wakeLock = power.newWakeLock(PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE, name)
        wakeLock?.acquire()
        super.onStart()
    }

    override fun callApplicationOnCreate(app: Application?) {
        super.callApplicationOnCreate(app)
        Utils.init(app)
        loadKoinModules(testModule)
    }

    override fun onDestroy() {
        super.onDestroy()
        wakeLock?.release()
    }

}