package com.cocosw.android

import com.cocosw.android.service.provideMemoryMockResponseSupplier
import com.cocosw.android.service.provideTestOptimus
import com.cocosw.optimus.MockResponseSupplier
import com.cocosw.optimus.Response
import org.koin.core.module.Module
import org.koin.dsl.module
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty1

val testModule: Module = module(override = true) {
    single { provideTestOptimus(get(), get(),get()) }
    single { provideMemoryMockResponseSupplier() }
}

