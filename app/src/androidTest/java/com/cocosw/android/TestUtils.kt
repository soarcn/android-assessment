package com.cocosw.android

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import com.cocosw.optimus.MockResponseSupplier
import com.cocosw.optimus.Response
import io.mockk.every
import io.mockk.mockk
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty1

fun mockLifecycleOwner(): LifecycleOwner {
    val owner: LifecycleOwner = mockk()
    val lifecycle = LifecycleRegistry(owner)
    lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    every { owner.lifecycle } returns lifecycle
    return owner
}

fun MockResponseSupplier.mock(method: KFunction<*>, value: KProperty1<*, Response<*>>) {
    save(method.toString(), value.name)
}
