package com.cocosw.android

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cocosw.android.ui.MainFragment

class MainActivity : AppCompatActivity(R.layout.root_layout) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, MainFragment())
                .commitNow()
        }
    }
}
