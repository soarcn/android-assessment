package com.cocosw.android

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.cocosw.android.domain.APINAME
import com.cocosw.android.domain.domainModule
import com.cocosw.android.ui.uiModule
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidApplication
import org.koin.core.qualifier.named
import org.koin.dsl.module

// Koin module
val appModule = module {
    single { provideSharedPreferences(this.androidApplication()) }
    single(named(APINAME.DEFAULT)) { provideOkHttpClient(this.androidApplication()) }
    single { providePicasso(get(), get(named(APINAME.DEFAULT))) }
} + domainModule + uiModule

private fun providePicasso(context: Context, client: OkHttpClient): Picasso {
    return Picasso.Builder(context).downloader(OkHttp3Downloader(client)).build()
}

private fun provideSharedPreferences(app: Application): SharedPreferences {
    return app.getSharedPreferences("android", Context.MODE_PRIVATE)
}

private fun provideOkHttpClient(app: Application): OkHttpClient {
    return createOkHttpClient(app).build()
}

fun createOkHttpClient(app: Application): OkHttpClient.Builder {
    return OkHttpClient.Builder()
}
