package com.cocosw.android.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cocosw.android.R
import com.cocosw.android.core.GridPresenter
import com.cocosw.android.core.StatePresenter
import com.cocosw.android.databinding.MainFragmentBinding
import com.cocosw.android.domain.DomainModel
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class MainFragment : Fragment(R.layout.main_fragment) {
    private lateinit var bind: MainFragmentBinding
    private val viewModel: MainViewModel by viewModel()
    private val statePresenter: StatePresenter by inject { parametersOf(bind.swipe) }
    private val gridPresenter: GridPresenter<DomainModel> by inject {
        parametersOf(
            bind.list,
            ListAdapter()
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = MainFragmentBinding.inflate(inflater, container, false)
        return bind.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        statePresenter.observe(viewModel, viewLifecycleOwner)
        gridPresenter.observe(viewModel.value, viewLifecycleOwner)
        bind.swipe.setOnRefreshListener {
            viewModel.load()
        }
        if (savedInstanceState == null) {
            viewModel.load()
        }
    }
}
