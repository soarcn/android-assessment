package com.cocosw.android.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cocosw.android.core.StateViewModel
import com.cocosw.android.domain.Domain
import com.cocosw.android.domain.DomainModel
import io.reactivex.Single

internal class MainViewModel(private val domain: Domain) : StateViewModel<List<DomainModel>>() {
    private val _value: MutableLiveData<List<DomainModel>> = MutableLiveData()
    internal val value: LiveData<List<DomainModel>> = _value

    override fun onNext(t: List<DomainModel>) {
        _value.value = t
    }

    override fun observable(): Single<List<DomainModel>> = domain.getModel()

    internal fun load() = go()
}
