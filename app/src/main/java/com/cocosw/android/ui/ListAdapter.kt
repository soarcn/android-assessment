package com.cocosw.android.ui

import androidx.recyclerview.widget.DiffUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseDataBindingHolder
import com.cocosw.android.R
import com.cocosw.android.databinding.LayoutListItemBinding
import com.cocosw.android.domain.DomainModel

class ListAdapter : BaseQuickAdapter<DomainModel, BaseDataBindingHolder<LayoutListItemBinding>>(
    R.layout.layout_list_item
) {
    init {
        setDiffCallback(
            object : DiffUtil.ItemCallback<DomainModel>() {
                override fun areItemsTheSame(oldItem: DomainModel, newItem: DomainModel): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: DomainModel,
                    newItem: DomainModel
                ): Boolean {
                    return oldItem.id == newItem.id
                }
            }
        )
    }

    override fun convert(holder: BaseDataBindingHolder<LayoutListItemBinding>, item: DomainModel) {
        holder.dataBinding?.item = item
    }
}
