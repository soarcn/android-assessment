package com.cocosw.android.ui

import androidx.browser.customtabs.CustomTabsIntent
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cocosw.android.core.GridPresenter
import com.cocosw.android.core.StatePresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val uiModule = module {
    viewModel { MainViewModel(get()) }
    factory { CustomTabsIntent.Builder().build() }
    factory { (view: SwipeRefreshLayout) -> StatePresenter(view) }
    factory { (view: RecyclerView, adapter: ListAdapter) -> GridPresenter(view, adapter, get()) }
}
