package com.cocosw.android.core

import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.cocosw.android.R
import com.cocosw.android.domain.DomainModel

/**
 * A Presenter observes the data from a LiveData and update to a RecyclerView with given Adapter
 * The presenter will set a Grid layout manger to the RecyclerView
 *
 */
class GridPresenter<T>(
    private val recyclerView: RecyclerView,
    private val listAdapter: BaseQuickAdapter<T, *>,
    private val customTab: CustomTabsIntent
) {

    fun observe(list: LiveData<List<T>>, lifecycleOwner: LifecycleOwner) {
        listAdapter.setOnItemClickListener{ _, _,  position ->
            itemClicked(listAdapter.data[position])
        }
        recyclerView.apply {
            adapter = listAdapter
            layoutManager = GridLayoutManager(context, resources.getInteger(R.integer.column))
        }
        list.observe(lifecycleOwner) {
            listAdapter.setList(it)
        }
    }

    private fun itemClicked(model:T) {
        Uri.parse((model as DomainModel).url).run {
            customTab.launchUrl(recyclerView.context, this)
        }
    }
}
