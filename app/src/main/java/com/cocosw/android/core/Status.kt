package com.cocosw.android.core

internal sealed class Status {
    object SUCCESS : Status()
    class ERROR(internal val e: Throwable) : Status()
    object LOADING : Status()
}
