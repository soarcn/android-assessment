package com.cocosw.android.core

import android.widget.ImageView
import androidx.core.view.isGone
import androidx.databinding.BindingAdapter
import com.cocosw.android.R
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun setImageUrl(view: ImageView, imageUrl: String?) {
    view.isGone = (imageUrl == null)
    if (imageUrl!=null)
        Picasso.get().load(imageUrl).placeholder(R.drawable.ic_sharp_portrait).into(view)
}
