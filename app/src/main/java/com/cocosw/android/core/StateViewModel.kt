package com.cocosw.android.core

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.schedulers.Schedulers

/**
 * StateViewModel use to simplify usage of Rxjava stream in a ViewModel and provide a state livedata for UI to observe the state of the steam
 * By providing the stream from observable() and trigger subscribe by calling go(), data will be emitted to onNext
 * In the meaning while the state will change from LOADING to SUCCESS or ERROR depends on the result of the stream
 * Stream will be disposed when the viewmodel been cleared
 */
abstract class StateViewModel<T> : ViewModel(), SingleObserver<T> {
    private var disposable: Disposable? = null
    internal val state = MutableLiveData<Status>()

    private fun error(e: Throwable) {
        state.value = Status.ERROR(e)
    }

    private fun complete() {
        state.value = Status.SUCCESS
    }

    internal fun go() {
        if (Status.LOADING != state.value) {
            state.value = Status.LOADING
            invoke()
        }
    }

    override fun onError(e: Throwable) {
        if (e is CompositeException)
            error(e.exceptions[0])
        else
            error(e)
    }

    override fun onSuccess(t: T) {
        complete()
        onNext(t)
    }

    abstract fun onNext(t: T)

    private fun invoke() {
        observable().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io())
            .subscribe(this)
    }

    override fun onSubscribe(d: Disposable) {
        disposable = d
    }

    abstract fun observable(): Single<T>

    override fun onCleared() {
        disposable?.dispose()
    }
}
