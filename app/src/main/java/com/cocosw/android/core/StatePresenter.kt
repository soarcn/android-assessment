package com.cocosw.android.core

import androidx.lifecycle.LifecycleOwner
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cocosw.android.R
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

/**
 * A presenter links the state in StateViewModel with a SwipeRefreshLayout
 * When state change to LOADING, change the isRefreshing to true, other wise reset to false
 * When state change to ERROR, presenter will show a Snackbar with generic error msg by default
 *
 */
class StatePresenter(private val view: SwipeRefreshLayout) {

    fun <T> observe(viewModel: StateViewModel<T>, lifecycleOwner: LifecycleOwner) {
        viewModel.state.observe(lifecycleOwner) {
            when (it) {
                is Status.ERROR -> onError(it.e)
                is Status.LOADING -> onLoading()
                is Status.SUCCESS -> onSuccess()
            }
        }
    }

    private fun onError(e: Throwable) {
        view.isRefreshing = false
        Snackbar.make(view, R.string.generic_error, Snackbar.LENGTH_SHORT).show()
        Timber.d(e)
    }

    private fun onLoading() {
        view.isRefreshing = true
    }

    private fun onSuccess() {
        view.isRefreshing = false
    }
}
