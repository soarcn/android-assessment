package com.cocosw.android

import android.app.Application
import org.koin.core.module.Module
import timber.log.Timber

object Injector {

    fun init(app: Application) {
        Timber.plant(Timber.DebugTree())
    }

    fun module(): List<Module> {
        return appModule
    }
}
