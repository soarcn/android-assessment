package com.cocosw.android

import android.app.Application
import com.cocosw.android.service.serviceMockModule
import org.koin.core.module.Module
import timber.log.Timber

object Injector {

    fun init(app: Application) {
    }

    fun module(): List<Module> {
        Timber.plant(Timber.DebugTree())
        return appModule +
            // development related module, don't forget put the module above to prod release Injector
            serviceMockModule + debugModule
    }
}
