package com.cocosw.android

import android.app.Application
import com.cocosw.android.domain.APINAME
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.X509TrustManager
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidApplication
import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.module
import timber.log.Timber

val debugModule: Module = module(override = true) {
    single(named(APINAME.DEFAULT)) { provideOkHttpClient(this.androidApplication()) }
    single { provideLoggingInterceptor() }
    single(named(APINAME.API)) { provideApiClient(get(named(APINAME.DEFAULT)), get()) }
}

private fun provideOkHttpClient(app: Application): OkHttpClient {
    return createOkHttpClient(app)
        .sslSocketFactory(createBadSSLSocketFactory(), permissive)
        .build()
}

private val permissive = object : X509TrustManager {
    override fun checkClientTrusted(p0: Array<out X509Certificate>?, p1: String?) {
    }

    override fun checkServerTrusted(p0: Array<out X509Certificate>?, p1: String?) {
    }

    override fun getAcceptedIssuers(): Array<X509Certificate?> {
        return arrayOfNulls(0)
    }
}

private fun createBadSSLSocketFactory(): SSLSocketFactory {
    val context = SSLContext.getInstance("TLS")
    context.init(null, arrayOf(permissive), null)
    return context.socketFactory
}

private fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val loggingInterceptor = HttpLoggingInterceptor(
        object : HttpLoggingInterceptor.Logger {
            override fun log(message: String) {
                Timber.tag("OkHttp").v(message)
            }
        }
    )
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return loggingInterceptor
}

private fun provideApiClient(
    client: OkHttpClient,
    loggingInterceptor: HttpLoggingInterceptor
): OkHttpClient {
    return client.newBuilder()
        .addInterceptor(loggingInterceptor)
        .build()
}
