package com.cocosw.android

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.app.AlertDialog
import com.cocosw.optimus.Optimus
import com.cocosw.optimus.OptimusView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class DebugActionButton @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FloatingActionButton(context, attrs, defStyleAttr), KoinComponent {
    private val optimus: Optimus by inject()

    init {
        setOnClickListener {
            AlertDialog.Builder(context)
                .setView(OptimusView(context).apply { this.setOptimus(optimus) })
                .create().show()
        }
    }
}
