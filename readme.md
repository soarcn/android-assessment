# Android Assessment App

This is An Android App developed for code assessment purpose. 

Architect
-------------

![Architect](arts/architect.png)

![DataFlow](arts/dataflow.png)

Getting Started
---------------
This project uses the Gradle build system. To build this project, use the
`gradlew build` command or use "Import Project" in Android Studio.

There are two Gradle tasks for testing the project:
* `connectedAndroidTest` - for running Espresso on a connected device
* `test` - for running unit tests

Libraries Used
--------------
* [Foundation][0] - Components for core system capabilities, Kotlin extensions and support for
  multidex and automated testing.
  * [AppCompat][1] - Degrade gracefully on older versions of Android.
  * [Android KTX][2] - Write more concise, idiomatic Kotlin code.
  * [Test][4] - An Android testing framework for unit and runtime UI tests.
* [Architecture][10] - A collection of libraries that help you design robust, testable, and
  maintainable apps. Start with classes for managing your UI component lifecycle and handling data
  persistence.
  * [Data Binding][11] - Declaratively bind observable data to UI elements.
  * [Lifecycles][12] - Create a UI that automatically responds to lifecycle events.
  * [LiveData][13] - Build data objects that notify views when the underlying database changes.
  * [ViewModel][17] - Store UI-related data that isn't destroyed on app rotations. Easily schedule
     asynchronous tasks for optimal execution.
* [UI][30] - Details on why and how to use UI Components in your apps - together or separate
  * [Fragment][34] - A basic unit of composable UI.
* Third party and miscellaneous libraries
  * [Picasso][90] for image loading
  * [Koin][92]: for [dependency injection][93]
  * [Rxjava][91] for managing background threads with simplified code and reducing needs for callbacks
  * [Optimus] for dynamic mocking
  
Screenshot
--------------
![Screenshot](arts/screenshot.png)
  
Submission Notes
----------------

* Code Compilation instructions; IDE/Plugin versions expected, dependency management

    Find it from Getting Started section

* Short description explaining architecture and logical modules its comprised of (e.g View, ViewModel...)

    Find it from Architect section

* Any 3rd party libraries used and rational

    Find it from Libraries Used

* Explain what each test does in comments or in document format
    
    Find the comment inline

* Any additional features -- apart the requirements given above
    * Pull to refresh
    * Local mock data is embedded and turn on by default in Debug build with a UI to change the mock behavior in runtime, press the action button to open the mock setting alert.
    