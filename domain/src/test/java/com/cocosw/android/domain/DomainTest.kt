package com.cocosw.android.domain

import com.cocosw.android.service.Api
import com.cocosw.android.service.Articles
import com.cocosw.android.service.Asset
import com.cocosw.android.service.RelatedImage
import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockk
import io.reactivex.Single
import org.junit.Test

class DomainTest {

    /**
     * Give api returns empty assets list, Domain returns empty list
     */
    @Test
    fun testEmptyArticles() {
        val api = mockk<Api>()
        every { api.get() } returns Single.just(Articles("", listOf()))

        val result = Domain(api).getModel().blockingGet()
        assertThat(result).isEmpty()
    }

    /**
     * The List that Domain returns should sorted by timeStamp in reverse order
     */
    @Test
    fun testArticleSortByTimestamp() {
        val api = mockk<Api>()
        every { api.get() } returns Single.just(
            Articles(
                "", listOf(
                    mockAsset(222),
                    mockAsset(111),
                    mockAsset(333)
                )
            )
        )

        val result = Domain(api).getModel().blockingGet()
        assertThat(result).hasSize(3)
        assertThat(result.first().timeStamp).isEqualTo(333)
        assertThat(result.last().timeStamp).isEqualTo(111)
    }

    /**
     * If asset doesn't contain images, domain object should contain a null image url
     */
    @Test
    fun testArticleWithOutImages() {
        val api = mockk<Api>()
        every { api.get() } returns Single.just(
            Articles(
                "", listOf(
                    mockAsset(222),
                    mockAsset(111),
                    mockAsset(333)
                )
            )
        )
        val result = Domain(api).getModel().blockingGet()
        assertThat(result.first().image).isNull()
    }

    /**
     * If asset has multiple images, return the smallest image
     */
    @Test
    fun testArticleWithImages() {
        val api = mockk<Api>()
        every { api.get() } returns Single.just(
            Articles(
                "", listOf(
                    mockAsset(222),
                    mockAsset(111),
                    mockAsset(
                        333, arrayListOf(
                            RelatedImage("1", "imageurl2", 222, 222),
                            RelatedImage("1", "imageurl1", 100, 100)
                        )
                    )
                )
            )
        )
        val result = Domain(api).getModel().blockingGet()
        assertThat(result.first().image).isEqualTo("imageurl1")
    }


    private fun mockAsset(timeStamp: Long = 999, images: ArrayList<RelatedImage>? = null): Asset {
        return Asset("1", "headline", "abstract", "line", timeStamp, images, "url")
    }
}
