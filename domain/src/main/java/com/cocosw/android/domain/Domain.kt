package com.cocosw.android.domain

import com.cocosw.android.service.Api
import io.reactivex.Single

open class Domain(private val api: Api) {
    open fun getModel(): Single<List<DomainModel>> {
        return api.get().map {
            it.assets
                .sortedByDescending { asset -> asset.timeStamp }
                .map { model ->
                DomainModel(
                    model.id,
                    model.headline,
                    model.theAbstract,
                    model.byLine,
                    model.url,
                    model.timeStamp,
                    model.relatedImages?.minOrNull()?.url
                )
            }
        }
    }
}
