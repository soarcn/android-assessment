package com.cocosw.android.domain

class DomainModel(
    val id:String,
    val headline: String,
    val theAbstract: String,
    val byLine:String,
    val url:String,
    val timeStamp: Long,
    val image:String?)
