package com.cocosw.android.domain

import com.cocosw.android.service.APINAME
import com.cocosw.android.service.serviceModule
import org.koin.dsl.module

object APINAME {
    const val API = APINAME.API
    const val DEFAULT = APINAME.DEFAULT
}

val domainModule = module {
    factory { Domain(get()) }
} + serviceModule
