package com.cocosw.android.service

import android.content.SharedPreferences
import com.cocosw.optimus.Converter
import com.cocosw.optimus.MockResponseSupplier
import com.cocosw.optimus.Optimus
import com.squareup.moshi.Moshi
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior
import java.util.concurrent.TimeUnit

val serviceMockModule = module(override = true) {
    single { provideOptimus(get(), get(), get()) }
    single { provideApiService(get()) }
}

private fun provideOptimus(
    sharedpreference: SharedPreferences,
    retrofit: Retrofit,
    moshi: Moshi
): Optimus {
    val supplier = MockResponseSupplier.create(sharedpreference)
    return Optimus.Builder(supplier)
        .retrofit(retrofit, sharedpreference)
        .mockGraph(MockApi)
        .converter(Converter.create(moshi))
        .build()
}

fun provideMemoryMockResponseSupplier(): MockResponseSupplier {
    return MockResponseSupplier.memory(hashMapOf())
}

fun provideTestOptimus(
    retrofit: Retrofit,
    moshi: Moshi,
    supplier :MockResponseSupplier
): Optimus {
    val networkBehavior = NetworkBehavior.create().apply {
        setDelay(0,TimeUnit.SECONDS)
        setFailurePercent(0)
        setVariancePercent(0)
    }
    val mockRetrofit = MockRetrofit.Builder(retrofit)
        .networkBehavior(networkBehavior).build()

    return Optimus.Builder(supplier)
        .mockRetrofit(mockRetrofit)
        .mockGraph(MockApi)
        .converter(Converter.create(moshi))
        .build()
}

private fun provideApiService(optimus: Optimus): Api {
    return optimus.create(Api::class.java)
}
