package com.cocosw.android.service

import com.cocosw.optimus.alter

internal val MockApi = alter(Api::class.java, "Api") {
    Api::get with MockArticles::class named "Get Articles"
}
