package com.cocosw.android.service

import android.content.Context
import com.cocosw.optimus.Default
import com.cocosw.optimus.MockResponse
import com.cocosw.optimus.error
import com.cocosw.optimus.success
import com.squareup.moshi.Moshi
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.get
import org.koin.core.context.GlobalContext

class MockArticles : MockResponse {
    @Default
    val Success = success { mockModel() }
    val HTTP404 = error(404) { ApiError(1, "Not Found") }
    val HTTP400 = error(400) { ApiError(1, "Api Error") }

    private fun mockModel(): Articles {
        val context:Context = GlobalContext.get().get()
        val moshi:Moshi = GlobalContext.get().get()
        return moshi.adapter(Articles::class.java).fromJson(context.assets.open("mock.json").bufferedReader().use { it.readText() })!!
    }

}

